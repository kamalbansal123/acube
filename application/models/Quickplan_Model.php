<?php 
class Quickplan_Model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
    function save_cutplan_data($data_obj){
    	$insert_cutplan_order_input_query_text = "INSERT INTO `cutplan_orders_input` 
                                                    (`factory`, 
                                                    `customer`, 
                                                    `product`, 
                                                    `order_number`, 
                                                    `color`, 
                                                    `max_plies`,
                                                    `recommended_lay_height`,
                                                    `max_pieces`, 
                                                    `min_pieces`, 
                                                    `carton_size`,
                                                    `number_of_sizes`, 
                                                    `marker_in_multiple`, 
                                                    `oversize_quantity`, 
                                                    `layplan_type`, 
                                                    `first_marker_as_max_pieces`, 
                                                    `allowed_time`, 
                                                    `allowed_solutions`, 
                                                    `forced_stepdown`, 
                                                    `remnant_contribution_ceiling`, 
                                                    `lay_completion_fraction`, 
                                                    `mode`, 
                                                    `size_sequence`, 
                                                    `order_quantity`, 
                                                    `max_colors_cut_together`, 
                                                    `express_solution_needed`, 
                                                    `ply_variance_limit`,is_auto_lcf,created_at) VALUES (
                                                    ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,NOW()
                                                    ); ";
        $this->db->query($insert_cutplan_order_input_query_text,array(
                                                    $data_obj['factory'],
                                                    $data_obj['customer'], 
                                                    $data_obj['product'],
                                                    $data_obj['order_number'],
                                                    $data_obj['color_sequence'],  
                                                  	$data_obj['max_plies'], 
                                                  	$data_obj['recommended_lay_height'],
                                                    $data_obj['max_pieces_in_marker'],  
                                                    $data_obj['min_pieces'],  
                                                    $data_obj['carton_size'],  
                                                    $data_obj['number_of_sizes'],  
                                                    $data_obj['marker_in_multiple'],  
                                                    $data_obj['oversize_quantity'], 
                                                    $data_obj['layplan_type'], 
                                                    $data_obj['first_marker_as_per_max_pcs'],  
                                                    $data_obj['allowed_time'],  
                                                    $data_obj['allowed_solutions'],  
                                                    $data_obj['forced_stepdown'],  
                                                    $data_obj['remnant_contribution_ceiling'], 
                                                    $data_obj['lay_completion_fraction'],
                                                    $data_obj['mode'],  
                                                    $data_obj['size_sequence'],  
                                                    $data_obj['order_quantity'],  
                                                    $data_obj['max_colors_cut_together'], 
                                                    $data_obj['express_solution_needed'],
                                                    $data_obj['ply_variance_limit'],
                                                    $data_obj['auto_lcf'])); 
        $insert_id = $this->db->insert_id();
        $job_id = $this->queue_cutplan($insert_id, $data_obj);
    	return $job_id;
    }
    public function result1($job_id){
	    $url = $this->getURL($job_id);
	    $ch = curl_init();   
	    curl_setopt_array($ch, array(
		    CURLOPT_RETURNTRANSFER => 1,
		    CURLOPT_URL => $url
		));                                                                                      
	    $result = curl_exec($ch);	
	    return $result;
	}

	public function getURL($job_id = "") {
	    if($job_id == "")
	        return 'http://'.$this->host .'/api/cutplans?token='. $this->token;
	    else
	        return 'http://'.$this->host. '/api/cutplans/' . $job_id .'?token='. $this->token;
	}
	private $service = 'stepdown_cutplan';
	private $requestor = 'intellocut';
	private $CI = null;
	private $host = null;
	private $token = null;

	public function queue_cutplan($insert_id, $data_obj) {
	        ini_set('memory_limit', '-1');
	        $this->CI =& get_instance();
	        $this->host = $this->CI->config->item('cutplan_host');
	        $this->token = $this->CI->config->item('cutplan_token');
	        // $this->CI->load->library('curl');
	        $input_orders_query_text = "SELECT * from cutplan_orders_input where id=?";
	        $input_orders_query = $this->db->query($input_orders_query_text,$insert_id);
	        foreach ($input_orders_query->result() as $cutplan) {
	            $cutplan_type = array();
	            $product_description = array();
	            $cutplan_type[3] = "stepdown_cutplan";
	            $fabric_color_map = array();
	                    $mode = $cutplan->mode;
	                    $input = new stdClass;
	                    $input->reference_id = $cutplan->factory . '-' . $cutplan->order_number;
	                    $input->colors = explode('-|-', $data_obj['color_sequence']);
	                    // $input->cutplan = array($cutplan);
	                    // populate priority map
	                    $input->ordering_priority_map = new StdClass;
	                    $input->ordering_priority_map->MARKER = (string)1;
	                    $input->ordering_priority_map->LAY = (string)2;
	                    $input->ordering_priority_map->NUM_BODIES = (string)3;
	                    $input->ordering_priority_map->BODIES_CUT = (string)4;
	                    $input->ordering_priority_map->PLY = (string)0;
	                        $cut_setting = new stdClass;
	                        $cut_setting->contribution = $cutplan->remnant_contribution_ceiling;                        
	                        $cut_setting->tolerence_percentage = "0.0";
	                        $cut_setting->max_pieces_in_marker = $cutplan->max_pieces;
	                        $cut_setting->first_marker_as_per_max_pcs = $cutplan->first_marker_as_max_pieces;
	                        $cut_setting->ply_variance_limit = (string)($cutplan->ply_variance_limit);

	                        $cut_setting->maximum_plies = $cutplan->max_plies;
	                        $cut_setting->cutplan_top_results =  (string)3;
	                       
	                        $cut_setting->express_solution_needed = $cutplan->express_solution_needed;

	                        $cut_setting->max_colors_cut_together = (string)($cutplan->max_colors_cut_together);
	                       
	                        $cut_setting->layplan_type = $cutplan->layplan_type;
	                        $cut_setting->end_loss = 0;
	                        $cut_setting->carton_size = $cutplan->carton_size;
	                        $cut_setting->mode = 3;
	                        if($cutplan->is_auto_lcf == 1)
	                       		 $cut_setting->is_all_lcf = true;
	                       	else
	                       		$cut_setting->is_all_lcf = false;
	                        $cut_setting->size_tolerance = 0;
	                        $cut_setting->product_description = new StdClass;
	                        $cut_setting->product_description->name = $cutplan->product;
	                        $cut_setting->brand_name = $cutplan->customer;	
	                        $stepdown_settings = new stdClass;
	                        if($cutplan->mode == 3) {
	                            $stepdown_settings->copy_cutplan_multiple = 1;
	                            $stepdown_settings->min_pieces_in_marker = $cutplan->min_pieces;
	                            $stepdown_settings->cutplan_allowed_time = $cutplan->allowed_time;
	                            $stepdown_settings->forced_stepdown = $cutplan->forced_stepdown;
	                            $stepdown_settings->cutplan_no_of_allowed_solutions = $cutplan->allowed_solutions;
	                            $stepdown_settings->even_pieces_markers = (string)1;
	                            $stepdown_settings->maximum_pieces_allowed_to_cut = $cutplan->oversize_quantity;
	                            $stepdown_settings->remanant_contribution_seiling = $cutplan->remnant_contribution_ceiling;
	                            $stepdown_settings->layout_completion_fraction = $cutplan->lay_completion_fraction;
	                            $stepdown_settings->marker_in_multiple = $cutplan->marker_in_multiple;
	                        }
	                        else {
	                            $stepdown_settings->copy_cutplan_multiple = 1;
	                            $stepdown_settings->min_pieces_in_marker = 1;
	                            $stepdown_settings->min_pieces_in_marker = 1;
	                            $stepdown_settings->cutplan_allowed_time = $cutplan->allowed_time;
	                            $stepdown_settings->forced_stepdown = $cutplan->forced_stepdown;
	                            $stepdown_settings->cutplan_no_of_allowed_solutions = $cutplan->allowed_solutions;
	                            $stepdown_settings->even_pieces_markers = (string)1;
	                            $stepdown_settings->maximum_pieces_allowed_to_cut = $cutplan->oversize_quantity;
	                            $stepdown_settings->remanant_contribution_seiling = $cutplan->remnant_contribution_ceiling;
	                            $stepdown_settings->layout_completion_fraction = $cutplan->lay_completion_fraction;
	                            $stepdown_settings->marker_in_multiple = $cutplan->marker_in_multiple;
	                        }

	                        if($data_obj['is_ann'] == 1){
		                        if($cutplan->mode == 4 && $data_obj['least_marker_mode_without_ann'])
		                       		$cut_setting->use_asymptotic = true;
		                       	else
		                       		$cut_setting->use_asymptotic = false;
		                    }
		                    else
		                    	$cut_setting->use_asymptotic = true;
	                       	
	                    $input->settings = $cut_setting;
	                    $input->stepdown_settings = $stepdown_settings;
	                    $input->size_details = array();
	                    $colors=array();
	                    $colors = explode('-|-', $data_obj['color_sequence']);
	                    $sizes = explode(',', $data_obj['size_sequence']);
	                    $quantities = explode(',', $data_obj['order_quantity']);
	                    $i = 0;
	                    foreach ($colors as $color) {
	                        $cutplan_request->colors[] = $color;
	                        // populate qty
	                        $input->size_details[$color] = array();
	                        foreach ($sizes as $key => $size){
                            	$input->size_details[$color][$size] = array();
                            	$input->size_details[$color][$size]['quantity'] = $quantities[$i];
                            	$input->size_details[$color][$size]['variance'] = 0;
                            	$i++;
                            	// var_dump($quantities[$i], $i);
	                        }
	                    }//die();
	                    $status = 0;
	                    //var_dump(json_encode($input)); die();
	                    // echo json_encode($result);
	                    $service=$mode;
	                    $params = array($input);
	                    if(null !== $service && $this->CI->config->item('cutplan_services_' . $service) != null)
	                        $this->service = $this->CI->config->item('cutplan_services_' . $service);

	                    $method = 'post';

	                    $input_params = array('params' => array('requestor' => $this->requestor, 'service' => $this->service, 'parameters' => $params));

	                    $url = $this->getURL();
	                    $input = json_encode($input_params);
	                    // var_dump($input); die();
	                    $ch = curl_init($url);                                                                      
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
						curl_setopt($ch, CURLOPT_POSTFIELDS, $input);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));                                                                                          
						$result = curl_exec($ch);
			
	     //                $this->CI->curl->create($url); //creating curl request url using curl library function.
	     //                $this->CI->curl->option(CURLOPT_HTTPHEADER, array('Content-type: application/json')); //passing
	     //                $this->CI->curl->post($input); //sending post request.
	     //                var_dump($this->CI->curl->post($input));die();
	                    $exec_curl = json_decode($result);  
	                    //var_dump($exec_curl);die();           
	                    // return json_encode($exec_curl);
	                    $output_data_json = json_encode($exec_curl);
	                    $output_data = json_decode($output_data_json);
	                    // var_dump($output_data); die();
	                    $version_id = 1;
	                    $job_id = null;
	                    //var_dump($output_data);die();
	                    if(null !== $output_data && "null" !== $output_data ) {
	                      // generate task queue reference
	                    	$job_id = $output_data->job_id;
	                      	$local_queue_query = "INSERT INTO `cutplan_orders_output` (
	                                                `version_id`,
	                                                `cutplan_orders_input_id`,
	                                                `factory`,
	                                                `customer`,
	                                                `product`,
	                                                `order_number`,
	                                                `number_of_sizes`,
	                                                `job_id`,
	                                                `status`,
	                                                `max_plies`,
	                                                `max_pieces`,
	                                                `min_pieces`,
	                                                `carton_size`,
	                                                `marker_in_multiple`,
	                                                `layplan_type`,
	                                                `mode`,
	                                                `forced_stepdown`,
	                                                `oversize_quantity`,
	                                                `remnant_contribution_ceiling`,
	                                                `lay_completion_fraction`,
	                                                `size_sequence`,
	                                                `order_quantity`,
	                                                `max_colors_cut_together`,
	                                                `ply_variance_limit`,
	                                                `express_solution_needed`,
	                                                `first_marker_as_max_pieces`,
	                                                `input`,
	                                                `output`,
	                                                `forced_stepdown_result`,
	                                                `allowed_solutions`,
	                                                `allowed_time`,
	                                                `all_lcf`,
	                                                `created_at`,
	                                                `updated_at`) VALUES (
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                ?,
	                                                NOW(),
	                                                NOW())";
	                      $this->db->query($local_queue_query, array(
	                                                $version_id,
	                                                $cutplan->id,
	                                                $cutplan->factory,
	                                                $cutplan->customer,
	                                                $cutplan->product,
	                                                $cutplan->order_number,
	                                                count($sizes),
	                                                $output_data->job_id,
	                                                0,
	                                                $cutplan->max_plies,
	                                                $cutplan->max_pieces,
	                                                $cutplan->min_pieces,
	                                                $cutplan->carton_size,
	                                                $cutplan->marker_in_multiple, 
	                                                0,
	                                                3,
	                                                $cutplan->forced_stepdown,
	                                                $cutplan->oversize_quantity,
	                                                $cutplan->remnant_contribution_ceiling,
	                                                $cutplan->lay_completion_fraction,
	                                                $cutplan->size_sequence,
	                                                $cutplan->order_quantity,
	                                                $cutplan->max_colors_cut_together,
	                                                $cutplan->ply_variance_limit,
	                                                $cutplan->express_solution_needed,
	                                                $cutplan->first_marker_as_max_pieces,
	                                                json_encode($input),
	                                                $output_data_json,
	                                                $data_obj['forced_stepdown'],
													$data_obj['allowed_solutions'],
	                                                $data_obj['allowed_time'],
													$data_obj['auto_lcf']
	                                                ));
	                      return $job_id;
	                    }
	                    else {
	                      return FALSE;
	                    }

	        }
	}
    function check_cutplan_data($order_number){
    	$query_text = "SELECT coo.result, coo.order_number, coi.* from cutplan_orders_output as coo INNER JOIN cutplan_orders_input as coi ON (coi.id = coo.cutplan_orders_input_id ) where coo.order_number = ? order by coo.id desc";
    	$query = $this->db->query($query_text, $order_number);
    	if($query->num_rows()){
    		$result = $query->result();
    		//var_dump($result);die();
    		return $result;
    	}
    	else{
    		return 0;
    	}
    }

    function is_cutplan_ready($job_id){
    	$query_text = "SELECT coo.result, coo.order_number, coi.* from cutplan_orders_output as coo INNER JOIN cutplan_orders_input as coi ON (coi.id = coo.cutplan_orders_input_id ) where coo.job_id = ?";
    	$query = $this->db->query($query_text, $job_id);
    	if($query->num_rows()){
    		$result = $query->result();
    		return $result;
    	}
    	else{
    		return 0;
    	}
    }

    function get_cutplan_settings($order_number){
    	$cutplan_settings = "SELECT * FROM cutplan_orders_input WHERE order_number = ? order by id desc LIMIT 0,1";
    	$query = $this->db->query($cutplan_settings, $order_number);
    	if($query->num_rows()){
    		return $query->result();
    	}
    	else{
    		return false;
    	}
    }
}
?>