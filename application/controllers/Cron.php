<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller {
	function check_cutplan_data(){
    	$query_text = "SELECT * from cutplan_orders_output where status = 0 order by id desc LIMIT 0,1";
        $update_task_queue = "UPDATE cutplan_orders_output 
                                    SET result = ?,
                                    per_piece_consumption=?, 
                                    number_of_markers=?, 
                                    number_of_lays=?, 
                                    markers_body_count=?, 
                                    number_of_plies=?, 
                                    execution_time=?, 
                                    total_consumption=?, 
                                    total_pieces_cut=?, 
                                    lcf_result=?,  
                                    result_net_file_name=?,status = 1 
                                    WHERE id = ? AND status = 0";

        $this->CI =& get_instance();
        $this->host = $this->CI->config->item('cutplan_host');
        $this->token = $this->CI->config->item('cutplan_token');
        $this->CI->load->library('curl');
        $query = $this->db->query($query_text);
        if($query->num_rows() > 0) {
            foreach ($query->result() as $value) {
                $data = $this->result1($value->job_id);
                $output = json_decode($data);
                if(count($output->cutplans) > 0) {
                    $cutplan = $output->cutplans[0];
                    $markers = $cutplan->markers;
                    $number_of_markers=count($markers);
                    $per_piece_consumption = 0; //Dummy data
                    $number_of_lays=0;
                    $markers_body_count=0;
                    $number_of_plies=0;
                    $execution_time = 0; //Dummy data
                    $total_consumption = 0; //Dummy data
                    $total_pieces_cut=0;
                    foreach ($markers as  $marker) {
                        $number_of_lays+=count($marker->lay_plies_maps);
                        $number_of_plies+=$marker->plies;
                        $markers_body_count+=$marker->no_of_bodies;
                        $total_pieces_cut+=$marker->plies*$marker->no_of_bodies;
                    }
                    $lay_completion_fraction = $cutplan->lay_completion_fraction;
                    $ann_file_name = $cutplan->ann_file_name;
                    $this->db->query($update_task_queue, 
                                        array(json_encode($output),
                                        $per_piece_consumption,
                                        $number_of_markers,
                                        $number_of_lays,
                                        $markers_body_count,
                                        $number_of_plies,
                                        $execution_time,
                                        $total_consumption,
                                        $total_pieces_cut,
                                        $lay_completion_fraction,
                                        $ann_file_name,
                                        $value->id));
                }
            }
        }
    }

    public function result1($job_id){
	    $url = $this->getURL($job_id);
	    $ch = curl_init();   
	    curl_setopt_array($ch, array(
		    CURLOPT_RETURNTRANSFER => 1,
		    CURLOPT_URL => $url
		));                                                                                      
	    $result = curl_exec($ch);	
	    return $result;
	}

	public function getURL($job_id = "") {
	    if($job_id == "")
	        return 'http://'.$this->host .'/api/cutplans?token='. $this->token;
	    else
	        return 'http://'.$this->host. '/api/cutplans/' . $job_id .'?token='. $this->token;
	}
}