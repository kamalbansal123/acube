<?php
Class Login extends CI_Controller {

	public function __construct() {
		parent::__construct();

		// Load form helper library
		$this->load->helper('form','url');

		// Load form validation library
		$this->load->library('form_validation');

		// Load database
		$this->load->model('Login_Database');
	}

	// Show login page
	public function index() {
		$this->load->view('login/login');
	}

	// Show registration page
	public function user_registration_show() {
		$this->load->view('registration_form');
	}

	// Check for user login process
	public function user_login_process() {
		$data = array(
		'username' => $this->input->post('username'),
		'password' => $this->input->post('password')
		);
		$result = $this->Login_Database->login($data);
		if ($result == TRUE) {
			$username = $this->input->post('username');
			$result = $this->Login_Database->read_user_information($username);
			if ($result != false) {
				$session_data = array(
				'username' => $result[0]->user_name,
				'email' => $result[0]->user_email,
				);
				// Add user data in session
				$this->session->set_userdata('logged_in', $session_data);
				redirect(site_url('quickplan/index'));
			}
		} else {
			$data = array(
			'error_message' => 'Invalid Username or Password'
			);
			$this->load->view('login/login', $data);
		}
	}
	// Logout from admin page
	public function logout() {
		$sess_array = array('username' => '');
		$this->session->unset_userdata('logged_in', $sess_array);
		$data['message_display'] = 'Successfully Logout';
		redirect('login/index');
	}
}
?>