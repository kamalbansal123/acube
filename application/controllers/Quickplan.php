<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//var_dump("test");
// var_dump($this->session->userdata['logged_in']);
// if (isset($this->session->userdata['logged_in'])) {
// $username = ($this->session->userdata['logged_in']['username']);
// $email = ($this->session->userdata['logged_in']['email']);
// } else {
// header("location: login/login");
// }
class Quickplan extends CI_Controller {

	private $template_data = array();
	public function index()
	{	
		$this->load->helper('url');
		$this->load->view('quickplan/quick_plan');
	}
	public function save_data(){
		$data_json_string = $this->input->post('data');
		$order_number = trim($data_json_string['order_number']);
        $this->load->model("Quickplan_Model");
        $status = $this->Quickplan_Model->save_cutplan_data($data_json_string);
		echo $status;
	}
	public function check_cutplan_data(){
		$order_number = $this->input->post('order_number');	
		$this->load->model("Quickplan_Model");
        $status = $this->Quickplan_Model->check_cutplan_data($order_number);
        $cutplan_settings = $this->Quickplan_Model->get_cutplan_settings($order_number);
        $result = array();
        if($status == 0){
        	echo 0;
        }else{
            $result['output'] = $status;
            $result['input'] = $cutplan_settings[0];
        	echo json_encode($result);
        }
	}
	public function is_cutplan_ready(){
		$job_id = $this->input->post('job_id');
		$this->load->model("Quickplan_Model");
		$status = $this->Quickplan_Model->is_cutplan_ready($job_id);
        if($status == 0){
        	echo 0;
        }else{
        	echo json_encode($status[0]);
        }
	}
}
