<?php


/**
  * General Asset Helper
  *
  * Helps generate links to asset files of any sort. Asset type should be the
  * name of the folder they are stored in.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @param		string    the asset type (name of folder)
  * @return		string    full url to asset
  */

// the directory containing the various asset type directories, including tailing slash
define('ASSETS_DIR', '');

define('DEFAULT_CSS_ASSET_TYPE', 'stylesheets');
define('DEFAULT_IMAGE_ASSET_TYPE', 'images');
define('DEFAULT_JS_ASSET_TYPE', 'javascripts');
define('DEFAULT_FLASH_ASSET_TYPE', 'flash');

function asset_path($asset_name, $asset_type = NULL)
{
	$asset_location = $asset_type.'/'.$asset_name;
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
    	return str_replace('http://', 'https://', base_url()) . $asset_location;
    }
    else {
    	return base_url() . $asset_location;
    }
}


// ------------------------------------------------------------------------

/**
  * Parse HTML Attributes
  *
  * Turns an array of attributes into a string
  *
  * @access		public
  * @param		array		attributes to be parsed
  * @return		string 		string of html attributes
  */

function _parse_asset_html($attributes = NULL)
{

	if(is_array($attributes)):
		$attribute_str = '';

		foreach($attributes as $key => $value):
			$attribute_str .= ' '.$key.'="'.$value.'"';
		endforeach;

		return $attribute_str;
	endif;

	return '';
}

// ------------------------------------------------------------------------

/**
  * CSS Asset Helper
  *
  * Helps generate CSS asset locations.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @return		string    full url to css asset
  */

function css_path($asset_name)
{
	return asset_path($asset_name, DEFAULT_CSS_ASSET_TYPE);
}


// ------------------------------------------------------------------------

/**
  * CSS Asset HTML Helper
  *
  * Helps generate JavaScript asset locations.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @param		string    optional, extra attributes
  * @return		string    HTML code for JavaScript asset
  */

function css_tag($asset_name, $attributes = array())
{
	$attribute_str = _parse_asset_html($attributes);
    $CI =& get_instance();
    $asset_version = $CI->config->item('asset_version');
    if($asset_version && $asset_version != '')
    	return '<link href="'.css_path($asset_name).'?' . $asset_version . '" rel="stylesheet" type="text/css"'.$attribute_str.'>';
    else
    	return '<link href="'.css_path($asset_name).'" rel="stylesheet" type="text/css"'.$attribute_str.'>';
}


function css_tag_custom($asset_name, $attributes = array())
{
  $CI =& get_instance();
  $CI->load->driver('minify');
  return $CI->minify->css->min('stylesheets/'.$asset_name);
}

// ------------------------------------------------------------------------

/**
  * Image Asset Helper
  *
  * Helps generate URI for image asset locations.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @return		string    full url to image asset
  */

function image_path($asset_name)
{
	return asset_path($asset_name, DEFAULT_IMAGE_ASSET_TYPE);
}


// ------------------------------------------------------------------------

/**
  * Image Asset HTML Helper
  *
  * Helps generate image HTML.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @param		string    optional, extra attributes
  * @return		string    HTML code for image asset
  */

function image_tag($asset_name, $attributes = array())
{
	$attribute_str = _parse_asset_html($attributes);

	return '<img src="'.image_path($asset_name).'"'.$attribute_str.'>';
}


// ------------------------------------------------------------------------

/**
  * JavaScript Asset URL Helper
  *
  * Helps generate JavaScript asset locations.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @return		string    full url to JavaScript asset
  */

function javascript_path($asset_name)
{
	return asset_path($asset_name, DEFAULT_JS_ASSET_TYPE);
}


// ------------------------------------------------------------------------

/**
  * JavaScript Asset HTML Helper
  *
  * Helps generate JavaScript asset locations.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @return		string    HTML code for JavaScript asset
  */

function javascript_tag($asset_name)
{

    $CI =& get_instance();
    $asset_version = $CI->config->item('asset_version');
    if($asset_version && $asset_version != '') {
      return '<script type="text/javascript" src="'.javascript_path($asset_name).'?' . $asset_version .'"></script>';    
    }
    else {
      return '<script type="text/javascript" src="'.javascript_path($asset_name).'"></script>';
    }
}


function javascript_tag_custom($asset_name)
{
    $CI =& get_instance();
    $CI->load->driver('minify');
    return $CI->minify->js->min('javascripts/'.$asset_name);
}


// ------------------------------------------------------------------------

/**
  * Flash Asset URL Helper
  *
  * Helps generate Flash asset locations.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @return		string    full url to Flash asset
  */

function flash_path($asset_name)
{
	return asset_path($asset_name, DEFAULT_FLASH_ASSET_TYPE);
}

// ------------------------------------------------------------------------

/**
  * RSS Icon in Addressbar Asset HTML Helper
  *
  * Helps generate image HTML.
  *
  * @access		public
  * @param		string    the name of the file or asset
  * @param		string    optional, extra attributes
  * @return		string    HTML code for rss asset
  */

function rss_tag($asset_url, $attributes = array())
{
	$attribute_str = _parse_asset_html($attributes);

	return '<link rel="alternate" href="' . $asset_url . '" type="application/rss+xml"' . $attribute_str . '>';
}
