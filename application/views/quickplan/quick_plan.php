<?php
//defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Cutplan Testing Tool</title>
	<script type="text/javascript" src = "../../public/javascript/acube_javascript.js"></script>
	<script type="text/javascript" src = "../../public/javascript/all.js"></script>
	<style type="text/css" src = "../../public/stylesheets/all_merged.css"></style>
	<style type="text/css" src = "../../public/stylesheets/intellocut.ordergrid.css"></style>
	<style type="text/css">
		body
	    {
	       background: #D3D3D3;
	    }
	    input 
		{
		    outline: none;
		    font-size: 20px;
		    font-family: "TW Cen MT";
		    padding: 4px 8px;
		    background: rgba(255,255,255,.4); 
		    border-radius: 2px;
		    border: none;
		    box-shadow: 0 0 6px rgba(0,0,0,.2);
		    transition:ease-out .1s;
		}
		button
		{
		    padding: 2px 8px;
		    font-family: "TW Cen MT";
		    font-size: 24px;
		    border: none;
		    margin-top: 10px;
		    box-shadow: 0 0 6px rgba(0,0,0,.2);
		    background: rgba(255,255,255,.4);
		    transition: ease-out .1s;
		    border-radius: 2px;
		}
		td.left{width:15%;}
		td.right{width:15%;}
		label{margin:10px 5px 15px 20px;
			font : .8em "typewriter", sans-serif;}
		/*input{margin:10px 5px 10px 10px;}
		.total-quantity{margin:10px 5px 10px 10px;}*/
		h4{text-align:center}
		td.marker_data{width: 100px;text-align: center;border: 1px solid #000000;}
		td.other_data{width: 200px; text-align: center;border: 1px solid #000000;}
		div.error {
		    color: #ff0000;
		}
		.floatLeft {width: 50%; float: left;}
		.floatRight {width: 50%; float: right;}
		.container { overflow: hidden;}
	</style>
</head>
<body>
	<div id="body">
		<div style="float:right;">
			<label><b>Search order</b></label>
			<input type="text" name="search_cut" id="search_cut" value="">
			<input id="search_cut_btn" type="button" value="Search" >
			<a id="logout" href=<?php echo base_url();?>login/logout> logout</a>
		</div>
		<div style="clear:both"></div>
		<div id="tablePrint"> </div>
		<div style="clear:both"></div>
		<div id="renderCutplans"> </div>
		<div style="clear:both"></div>
		<div class = "error" id = "settings_error_list">
			<ul id="error-list"> </ul>
		</div>
		<table align="center" style="display: block;position: absolute;">
		<form>
			<tr>
				<td class="left">
					<label>Factory</label>
				</td>
				<td>
					<input type="text" name="factory" id="factory" value="">
				</td>
				<td class="right">
					<label>Customer</label>
				</td>
				<td>
					<input type="text" name="customer" id="customer" value="">
				</td>
			</tr>
			<tr>
				<td class="left">
					<label>Product</label>
				</td>
				<td>
					<input type="text" name="product" id="product" value="">
				</td>
				<td class="right">
					<label>Order Number</label>
				</td>
				<td>
					<input type="text" name="order_number" id="order_number" value="">
				</td>
			</tr>	
			<tr>
				<td class="left">
					<label>Recommended lay height</label>
				</td>
				<td>
					<input type="text" name="recommended_lay_height" id="recommended_lay_height" value=""> 
				</td>
				<td class="right">
					<label>Maximum lay height</label>
				</td>
				<td>
					<input type="text" name="max_plies" id="max_plies" value="">
				</td>
			</tr>
			<tr>
				<td class="left">
					<label>Maximum Pieces in marker</label>
				</td>
				<td>
					<input type="text" name="max_pieces_in_marker" id="max_pieces_in_marker" value="">
				</td>
				<td class="right">
					<label>Minimum Pieces in marker</label>
				</td>
				<td>
					<input type="text" name="min_pieces" id="min_pieces" value="">
				</td>
			</tr>
			<tr>
				<td class="left">
					<label>Carton Size</label>
				</td>
				<td>
					<input type="text" name="carton_size" id="carton_size" value="">
				</td>
				<td class="right">
					<label>Marker in multiple</label>
				</td>
				<td>
					<input type="text" name="marker_in_multiple" id="marker_in_multiple" value="">
				</td>
			</tr>
			<tr>
				<td class="left">
					<label>Oversize Quantity</label>
				</td>
				<td>
					<input type="text" name="oversize_quantity" id="oversize_quantity" value="">
				</td>
				<td class="right">
					<label>Layplan Type</label>
				</td>
				<td>
					<input type="radio" name="layplan_type" checked id="use_average_lay_size" value="0"><label>Use average lay size</label>
					<input type="radio" name="layplan_type" id="use_max_lay_size" value="1"><label>Use max lay size</label>
				</td>
			</tr>
			<tr>
				<td class="left">
					<label>First Marker as Max pieces</label>
				</td>
				<td>
					<input type="checkbox" name="first_marker_as_max_pieces" id="first_marker_as_max_pieces" value="">
				</td>
				<td class="right">
					<label>Allowed Time</label>
				</td>
				<td>
					<input type="text" name="allowed_time" placeholder="10" id="allowed_time" value="10">
				</td>
			</tr>
			<tr>
				<td class="left">
					<label>Allowed Solutions</label>
				</td>
				<td>
					<input type="text" placeholder="10000" name="allowed_solutions" id="allowed_solutions" value="10000">
				</td>
				<td class="right">
					<label>Forced Stepdown</label>
				</td>
				<td>
					<input type="checkbox" name="forced_stepdown" id="forced_stepdown" value="">
				</td>
			</tr>
			<tr>
				<td class="left">
					<label>Remnant Contribution Ceiling</label>
				</td>
				<td>
					<input type="text" name="remnant_contribution_ceiling" id="remnant_contribution_ceiling" value="">
				</td>
				<td class="right">
					<label>Lay Completion Fraction</label>
				</td>
				<td>
					<input type="text" name="lay_completion_fraction" id="lay_completion_fraction" value="">
					<label>Should be less than 0.99</label>
				</td>
			</tr>
			<tr>
				<td class="left">
					<label>Mode</label>
				</td>
				<td>
            		<input name="cutplan_mode" id="stepdown-cutplan-mode" checked value="3" type="radio"><label>StepDown</label>       
            		<input name="cutplan_mode" id="normal-cutplan-mode" value="1" type="radio"><label>Normal</label>                
            		<input name="cutplan_mode" id="express-cutplan-mode" value="2" type="radio"><label>Express</label>
            		<input name="cutplan_mode" id="Least-marker-mode" value="4" type="radio"><label>Least marker</label>
        		</td>
            	<td class="right">
					<label>Auto LCF</label>
				</td>
				<td>
            		<input name="auto_lcf" id="auto_lcf_off" checked value="0" type="radio"><label>OFF</label>               
            		<input name="auto_lcf" id="auto_lcf_on" value="1" type="radio"><label>ON</label>              
            	</td>
            </tr>
			<tr>
				<td class="left">
					<label>Max Colors Cut Together</label>
				</td>
				<td>
					<input type="radio" name="max_colors_cut_together" checked id="lay_all_color_together" value="-1"><label>Lay all colors together</label>
					<input type="radio" name="max_colors_cut_together" id="lay_all_color_saperately" value="0"><label>Lay all colors separately</label>
				</td>
				<td class="right">
					<label>Express Solution Needed</label>
				</td>
				<td>
					<input type="checkbox" name="express_solution_needed" id="express_solution_needed" value="">
				</td>
			</tr>
			<tr>
				<td class="left">
					<label>Least Marker Mode without ANN</label>
				</td>
				<td>
					<input type="checkbox" name="least_marker_mode_without_ann" id="least_marker_mode_without_ann" value="">
				</td>
			</tr>
			<tr>
				<td class="left">
					<label>Is ANN</label>
				</td>
				<td>
            		<input name="is_ann" id="is_ann_off" checked value="0" type="radio"><label>OFF</label>               
            		<input name="is_ann" id="is_ann_on" value="1" type="radio"><label>ON</label>              
            	</td>
			<tr>
				<td colspan="4">
					<div style="float:none;overflow:auto;" id="new-order-grid"></div>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input id="save_cutplan_data" type="button" value="Submit" >
				</td>
			</tr>
			</tr>
		</form>
	</table>
	</div>
	
</body>
</html>

<script type="text/javascript">
	var base_url = '<?php echo base_url(); ?>';
	$(document).ready(function(){
		$('#new-order-grid').html('');
        $('#new-order-grid').ordergrid({});
	});
	var url = base_url+'index.php/quickplan/save_data';
	$('#save_cutplan_data').click(function(){
		var errors = new Array;
		$('#settings_error_list').empty();
		$('#settings_error_list').append('<ul id="error-list"> </ul>');
		var data_set_object = new Object();
		data_set_object.factory = $('#factory').val();
		data_set_object.customer = $('#customer').val();
		data_set_object.product = $('#product').val();
		data_set_object.order_number = $('#order_number').val();
		// data_set_object.color = $('#color').val();
		data_set_object.max_plies = $('#max_plies').val();
		data_set_object.max_pieces_in_marker = $('#max_pieces_in_marker').val();
		data_set_object.min_pieces = $('#min_pieces').val();
		data_set_object.carton_size = $('#carton_size').val();
		data_set_object.marker_in_multiple = $('#marker_in_multiple').val();
		data_set_object.oversize_quantity = $('#oversize_quantity').val();
		data_set_object.layplan_type = $('input[name=layplan_type]:checked').val();
		if($('#first_marker_as_max_pieces').is(":checked"))
			data_set_object.first_marker_as_per_max_pcs = 1;
		else
			data_set_object.first_marker_as_per_max_pcs = 0;
		data_set_object.allowed_time = $('#allowed_time').val();
		data_set_object.allowed_solutions = $('#allowed_solutions').val();
		if($('#forced_stepdown').is(":checked"))
			data_set_object.forced_stepdown = 1;
		else
			data_set_object.forced_stepdown = 0;
		data_set_object.remnant_contribution_ceiling = $('#remnant_contribution_ceiling').val();
		data_set_object.lay_completion_fraction = $('#lay_completion_fraction').val();
		data_set_object.mode = $('input[name=cutplan_mode]:checked').val();
		data_set_object.auto_lcf = $('input[name=auto_lcf]:checked').val();
		// data_set_object.size_sequence = $('#size_sequence').val();
		// data_set_object.order_quantity = $('#order_quantity').val();
		data_set_object.max_colors_cut_together = $('input[name=max_colors_cut_together]:checked').val();
		data_set_object.express_solution_needed = $('#express_solution_needed').is(":checked");
		data_set_object.recommended_lay_height = $('#recommended_lay_height').val();
		data_set_object.ply_variance_limit = parseInt(data_set_object.max_plies) - parseInt(data_set_object.recommended_lay_height);
		data_set_object.least_marker_mode_without_ann = $('#least_marker_mode_without_ann').is(':checked');
		data_set_object.is_ann = $('input[name=is_ann]:checked').val();
		
		var orderArray = new Array();
        var pickPosition = new Array();
		var counter = 1;
        var isSizeRepeated = false;
        var sizeInterimCounter = 0;
        $('.order-grid-size-text', $('#new-order-grid')).each(function(){
            if('' != $.trim($(this).val())) {
                var row_count = $("tr", obj).length - 2;
                var tmpArray = new Array();
                for(i=1; i< row_count; i++) {
                    var value = $("tr:eq(" + (2+i) + ") td:nth-child(" + (sizeInterimCounter + 3) + ") input.ordergrid-qty-" + i, obj).val();
                    if($.trim(value) != '' && parseInt($.trim(value)) > 0)
                        tmpArray.push($.trim(value));
                }
                var uTmpArray=tmpArray.filter(function(itm,i,tmpArray){
                    return i==tmpArray.indexOf(itm);
                });
                if(uTmpArray.length < 1  || (uTmpArray.length == 1 && uTmpArray[0] == 0)) {
                    // do nothing
                }
                else {
                    if($.inArray($.trim($(this).val()), orderArray) >= 0)
                        isSizeRepeated = true;
                    orderArray.push($(this).val());
                    pickPosition.push(sizeInterimCounter+1);
                }                
            }
            sizeInterimCounter++;
        });
        if(isSizeRepeated) {
            errors.push('Repeated sizes not allowed');
        }
        var colorArray = new Array();
        var colorCodeArray = new Array();
        counter = 1;
        var quantityArray = new Array();
        var isColorRepeated = false;
        var isColorExit = false;
        $('.order-grid-color-text', $('#new-order-grid')).each(function(){
            if('' != $.trim($(this).val())) {

                if($.inArray($(this).val(), colorArray) >= 0)
                    isColorRepeated = true;
                var tmpPickPosition = 0;
                var sizeExistCounter = 1;
                var tmpQuantityArray = new Array();
                $('.ordergrid-qty-' + counter, $('#new-order-grid')).each(function(){
                    if(pickPosition.length > 0){
                        if(undefined != pickPosition[tmpPickPosition] && pickPosition[tmpPickPosition] == sizeExistCounter) {
                            tmpPickPosition++;
                            if('' != $.trim($(this).val()))
                                tmpQuantityArray.push($(this).val());
                            else
                                tmpQuantityArray.push(0);
                        }
                    }
                    sizeExistCounter++;
                });
                var uTmpQuantityArray=tmpQuantityArray.filter(function(itm,i,tmpQuantityArray){
                    return i==tmpQuantityArray.indexOf(itm);
                });
                if(uTmpQuantityArray.length == 1 && uTmpQuantityArray[0] == 0) {
                    // do nothing
                }
                else {
                    colorArray.push($(this).val());
                    // console.log(ic.GLOBAL_COLOR_STRING_ALLOWED.test($(this).next().val()));
                    if(ic.GLOBAL_COLOR_STRING_ALLOWED.test($(this).next().val())){
                        colorCodeArray.push($(this).next().val());
                    }
                    else{
                        colorCodeArray.push("#ffffff");
                    }
                    quantityArray = $.merge(quantityArray, tmpQuantityArray);
                }
            }
            else{
                $('.ordergrid-qty-' + counter, $('#new-order-grid')).each(function(){ 
                    if('' != $.trim($(this).val())) {
                        isColorExit = true;                        
                    }                    
                });
            }
            counter++;
        });
        if(isColorRepeated) {
            errors.push("Repeated colors not allowed");
        }
        if(isColorExit) {
            errors.push("Color field cannot be blank");
        }
        data_set_object.number_of_sizes = orderArray.length;
        data_set_object.size_sequence = orderArray.join(',');
		data_set_object.order_quantity = quantityArray.join(',');
		data_set_object.color_sequence = colorArray.join('-|-');

		//Validations
		if(isNaN(parseInt(data_set_object.max_pieces_in_marker)) || parseInt(data_set_object.max_pieces_in_marker) == 0) {
            errors.push('Max pieces in marker cannot be blank or zero.');
        }
        if(isNaN(parseInt(data_set_object.min_pieces)) || parseInt(data_set_object.min_pieces) < 1) {
            errors.push('Min pieces in marker cannot be blank or less than 1.');
        }
        if(isNaN(parseInt(data_set_object.max_plies)) || parseInt(data_set_object.max_plies) == 0) {
            errors.push('Max plies cannot be blank or zero');
        }
        if(parseInt(data_set_object.recommended_lay_height) > parseInt(data_set_object.max_plies)) {
            errors.push('Recommended lay height cannot be greater than maximum lay height');
        }
		if(parseInt(data_set_object.min_pieces) > parseInt(data_set_object.max_pieces_in_marker)) {
            errors.push('Min pieces in marker cannot be greater than Max pieces in marker');
        }
        if(parseInt(data_set_object.carton_size) < 0) {
           	errors.push('Carton size cannot be less than 0');
        }
        if(parseInt(data_set_object.carton_size) > parseInt(data_set_object.max_plies)) {
           	errors.push('Carton size cannot be greater than max lay hieght')
        }
        if(parseInt(data_set_object.marker_in_multiple) < 1) {
            errors.push('Min pieces in marker in multiple cannot be less than 1');
        }
        if(parseInt(data_set_object.marker_in_multiple) > parseInt(data_set_object.max_pieces_in_marker)) {
            errors.push('Marker in multiple cannot be greater than max pieces in marker');
        }
        //console.log(errors.join('\n'));
        if(errors.length > 0){
        	for(i=0; i< errors.length; i++) {
                $("#error-list").append('<li>' + errors[i] + '</li>');
            }
        	return
        }

		$.post(url, {
				data : data_set_object
		},function(response){
			if(response != false){
				alert("Data Saved successfully");
				$("input[type=text]").val("");
				$("div.total-quantity").empty();
				$("#allowed_solutions").val('10000');
				$('#allowed_time').val('10');
				pollAXService(response);
			}
			else{
				alert("Duplicate order");
			}
		});
	});

	function pollAXService(job_id){
		var myInterval = setInterval(function() {
	        $.post(base_url+'index.php/quickplan/is_cutplan_ready', {job_id : job_id}, function(data) {
	            if(data.result != null) {
	            	clearInterval(myInterval);
	                renderCutplan(data);
	            }
	    },'json');},10000); 
	};

	function renderCutplan(result){
		var dataObject = new Object;
		var parsed_data = new Array;
		dataObject = JSON.parse(result.result);
		parsed_data = dataObject.cutplans;
		var sizes_header = result.size_sequence.split(',');
		if(parsed_data.length > 0)
		{
			var myTable= "<div class='container'><div class='floatLeft'>";
			myTable+="<h4>Order Number: "+result.order_number+"</h4>";
			myTable+= "<table class='marker_table' style='display: block;overflow-x: auto;white-space: nowrap;' align='center' ><tr><td class='marker_data'>S.NO</td>";
		    myTable+= "<td class='marker_data'>Marker String</td>";
		    for(z = 0; z < sizes_header.length; z++)
	    		myTable+="<td class='marker_data'>"+ sizes_header[z] +"</td>";
		    myTable+="<td class='marker_data'>No of bodies</td>";
		    myTable+="<td class='marker_data'>Plies</td>";
		    myTable+="<td class='marker_data'>Contribution</td></tr>";

		    var settingTable= "</div><div class='floatRight'><table class='marker_table' align='center'>";
		  	for(key in parsed_data){
				var marker_data = parsed_data[key].markers;
				var i=1;
				myTable+="<h4>Marker Details:</h4>";
				for(marker in marker_data){
					var marker_string = marker_data[marker].marker_string.split('|');
					var new_string = new Array;
					var sizeContribution = new Array;
					for(var j = 0; j < marker_string.length; j++){
						var sizeData = new Object;
						var string = marker_string[j].split('-');
						sizeData.name = string[1];
						sizeData.ratio = string[0];
						sizeContribution.push(sizeData);
						if(parseInt(string[0]) != 0)
							new_string.push(marker_string[j]);
					}
					// console.log(sizeContribution);
					myTable+="<tr><td class='marker_data'> " + i + " </td>";
				    myTable+="<td class='marker_data'>" + new_string.join(',') + "</td>";
				    for(k = 0; k < sizeContribution.length; k++){
			    		if(parseInt(sizeContribution[k].ratio) != 0)
			    			myTable += "<td class='marker_data'>" + sizeContribution[k].ratio + "</td>";
			    		else
			    			myTable += "<td class='marker_data'></td>";
				    }
				    myTable+="<td class='marker_data'>" +marker_data[marker].no_of_bodies + "</td>";
				    myTable+="<td class='marker_data'>" + marker_data[marker].plies + "</td>";
				    myTable+="<td class='marker_data'>" + marker_data[marker].contribution + "</td>";
				    myTable+="</tr>";
					i++;	
				}
				var layplan_type = "";
				if(result.layplan_type == 0){
					layplan_type = "Use average lay size";
				}else{
					layplan_type = "Use max lay size";
				}
				var first_marker_as_max_pieces = "";
				if(result.first_marker_as_max_pieces == 0){
					first_marker_as_max_pieces = "Unchecked";
				}else{
					first_marker_as_max_pieces = "Checked";
				}
				var forced_stepdown = "";
				if(result.forced_stepdown == 0){
					forced_stepdown = "Unchecked";
				}else{
					forced_stepdown = "Checked";
				}
				var mode_used = "";
				if(result.mode == 1){
					mode_used = "Normal";
				}else if(result.mode == 2){
					mode_used = "Express";
				}else if(result.mode == 3){
					mode_used = "StepDown";
				}else if(result.mode == 4){
					mode_used = "Least marker";
				}
				var is_auto_lcf = "";
				if(result.is_auto_lcf == 0){
					is_auto_lcf = "OFF";
				}else if(result.is_auto_lcf == 1){
					is_auto_lcf = "ON";
				}
				var max_colors_cut_together = "";
				if(result.max_colors_cut_together == -1){
					max_colors_cut_together = "Lay all colors together";
				}else if(result.max_colors_cut_together == 0){
					max_colors_cut_together = "Lay all colors separately";
				}
				settingTable+= "<h4></h4>";
				settingTable+= "<h4>Order Details:</h4>";
				settingTable+= "<tr><td class='other_data'>Recommended lay height</td>";
				settingTable+="<td class='marker_data'>" + result.recommended_lay_height + "</td>";
				settingTable+= "<td class='other_data'>Maximum lay height</td>";
				settingTable+="<td class='marker_data'>" + result.max_plies + "</td></tr>";
				settingTable+= "<tr><td class='other_data'>Maximum Pieces in marker</td>";
				settingTable+="<td class='marker_data'>" + result.max_pieces + "</td>";
				settingTable+= "<td class='other_data'>Minimum Pieces in marker</td>";
				settingTable+="<td class='marker_data'>" + result.min_pieces + "</td></tr>";
				settingTable+= "<tr><td class='other_data'>Carton Size</td>";
				settingTable+="<td class='marker_data'>" + result.carton_size  + "</td>";
				settingTable+= "<td class='other_data'>Marker in multiple</td>";
				settingTable+="<td class='marker_data'>" + result.marker_in_multiple  + "</td></tr>";
				settingTable+= "<tr><td class='other_data'>Oversize Quantity</td>";
				settingTable+="<td class='marker_data'>" + result.oversize_quantity + "</td>";
				settingTable+= "<td class='other_data'>Layplan Type</td>";
				settingTable+="<td class='marker_data'>" + layplan_type + "</td></tr>";
				settingTable+= "<tr><td class='other_data'>First Marker as Max pieces</td>";
				settingTable+="<td class='marker_data'>" + first_marker_as_max_pieces + "</td>";
				settingTable+= "<td class='other_data'>Allowed Time</td>";
				settingTable+="<td class='marker_data'>" + result.allowed_time  + "</td></tr>";
				settingTable+= "<tr><td class='other_data'>Allowed Solutions</td>";
				settingTable+="<td class='marker_data'>" + result.allowed_solutions + "</td>";
				settingTable+= "<td class='other_data'>Forced Stepdown</td>";
				settingTable+="<td class='marker_data'>" + forced_stepdown + "</td></tr>";
				settingTable+= "<tr><td class='other_data'>Remnant Contribution Ceiling</td>";
				settingTable+="<td class='marker_data'>" + result.remnant_contribution_ceiling + "</td>";
				settingTable+= "<td class='other_data'>Lay Completion Fraction</td>";
				settingTable+="<td class='marker_data'>" + result.lay_completion_fraction  + "</td></tr>";
				settingTable+= "<tr><td class='other_data'>Mode</td>";
				settingTable+="<td class='marker_data'>" + mode_used + "</td>";
				settingTable+= "<td class='other_data'>Auto LCF</td>";
				settingTable+="<td class='marker_data'>" + is_auto_lcf + "</td></tr>";
				settingTable+= "<tr><td class='other_data'>Max Colors Cut Together</td>";
				settingTable+="<td class='marker_data'>" + max_colors_cut_together  + "</td>";
				settingTable+= "<td class='other_data'>Express Solution Needed</td>";
				settingTable+="<td class='marker_data'>" + result.express_solution_needed + "</td></tr>";
				settingTable+= "<tr><td class='other_data'>Using ann</td>";
				settingTable+="<td class='marker_data'>" + parsed_data[key].using_ann + "</td>";
				settingTable+= "<td class='other_data'>Lay Completion Fraction</td>";
				settingTable+="<td class='marker_data'>" + parsed_data[key].lay_completion_fraction + "</td>";
				settingTable+= "<tr><td class='other_data'>Ann File name</td>";
				settingTable+="<td class='marker_data'>" + parsed_data[key].ann_file_name + "</td>";
				settingTable+= "<td class='other_data'>Created date</td>";
				settingTable+="<td class='marker_data'>" + result.created_at + "</td></tr>";
			}
		    
		   	myTable+="</table>";
		   	settingTable+="</table></div></div>";
		   	myTable +=settingTable;
		 	//document.write(myTable);
		 	$('#renderCutplans').append(myTable);
		}
		else{
			alert("No cutplan found");
		}
	};

	$('#search_cut_btn').click(function(){
		var order_number = $('#search_cut').val();
		$.post(base_url+'index.php/quickplan/check_cutplan_data', {
				order_number : order_number
		},function(response){
			if(response != 0){
				dataObject = JSON.parse(response);
				var output_length = dataObject.output.length;
				for(var i=0; i < output_length; i++){
					if(dataObject.output[i].result != null)
						renderCutplan(dataObject.output[i]);
				}
				data = dataObject.input;
				$('#factory').val(data.factory);
				$('#customer').val(data.customer);
				$('#product').val(data.product);
				$('#order_number').val(data.order_number);
				$('#max_plies').val(data.max_plies);
				$('#max_pieces_in_marker').val(data.max_pieces);
				$('#min_pieces').val(data.min_pieces);
				$('#carton_size').val(data.carton_size);
				$('#marker_in_multiple').val(data.marker_in_multiple);
				$('#oversize_quantity').val(data.oversize_quantity);
				$('#allowed_time').val(data.allowed_time);
				$('#allowed_solutions').val(data.allowed_solutions);
				$('#remnant_contribution_ceiling').val(data.remnant_contribution_ceiling);
				$('#lay_completion_fraction').val(data.lay_completion_fraction);
				$('#recommended_lay_height').val(data.recommended_lay_height);
				if(data.layplan_type == 0)
					$('#use_average_lay_size').prop('checked', true);
				else
					$('#use_max_lay_size').prop('checked', true);
				if(data.first_marker_as_max_pieces == 1)
					$('#first_marker_as_max_pieces').prop('checked', true);
				else
					$('#first_marker_as_max_pieces').prop('checked', false);
				if(data.forced_stepdown == 1)
					$('#forced_stepdown').prop('checked', true);
				else
					$('#forced_stepdown').prop('checked', false);
				if(data.mode == 3)
					$('#stepdown-cutplan-mode').prop('checked', true);
				else if(data.mode == 1)
					$('#normal-cutplan-mode').prop('checked', true);
				else
					$('#express-cutplan-mode').prop('checked', true);
				if(data.is_auto_lcf == 0)
					$('#auto_lcf_off').prop('checked', true);
				else
					$('#auto_lcf_on').prop('checked',true);
				if(data.max_colors_cut_together == -1)
					$('#lay_all_color_together').prop('checked', true);
				else
					$('#lay_all_color_saperately').prop('checked', true);
				if(data.express_solution_needed == "true")
					$('#express_solution_needed').prop('checked', true);
				else
					$('#express_solution_needed').prop('checked', false);

				var sizes = data.size_sequence.split(',');
				var quantity = data.order_quantity.split(',');
				var colors = data.color.split('-|-');
				var j = 0;
				var counter = 1;
				$('.order-grid-color-text , .order-grid-size-text , .ordergrid-qty').val("");
				$('.order-grid-color-text', $('#new-order-grid')).each(function(index){
					if(colors.length > index){
						$(this).val(colors[index]);
						$('.order-grid-size-text', $('#new-order-grid')).each(function(key){
							if(sizes.length > key){
								$(this).val(sizes[key]);
							}
							else
								return false;
						})
						$('.ordergrid-qty-' + counter, $('#new-order-grid')).each(function(i){
							if(sizes.length > i){
								$(this).val(quantity[j++]);
							}
							else
								return false;
						})
						counter++;
					}
					else
						return false;
				})
			 }else{
			 	alert("No Order found");
			 }
			
		});
	});
</script>