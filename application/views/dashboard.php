<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>acube</title>


</head>
<body>

<div id="container">
	<h1>Dashboard</h1>

	<div id="body">
		<input type="button" id = "quick_plan" value="Quick Plan"/>
		<button>Cut plan Analysis</button>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>

</body>
</html>
<script type="text/javascript" src = "../../public/javascript/acube_javascript.js"></script>
<script type="text/javascript">
	var base_url = '<?php echo base_url(); ?>'
	$('#quick_plan').click(function(){
		$.post(base_url +'dashboard/quick_plan_view', {
		}, function(){}, 'html');
	});
</script>
