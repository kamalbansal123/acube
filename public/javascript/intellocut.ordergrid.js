(function($){
    $.fn.ordergrid = function(options) {
        options = $.extend({
            dataUrl: '',
            dataParams : {},
            colors: 5,
            sizes: 10,
            type: 'full',
            newColorLink: true,
            sizeAutosuggestUrl : 'size/autosuggest',
            colorAutosuggestUrl : 'color/autosuggest',
            colorsData: {},/*{{id:id,name:name,color:color},{id:id,name:name,color:color}}*/
            sizesData: {},/*{{id:id,name:name},{id:id,name:name}}*/
            quantity: {},
            addNewColumnStartText : 'Add new column',
            addNewColumnEndText : 'Add new column',
            addNewRowText : 'Add new row'
        }, options);
        options.startIndex = 0;
        options.endIndex = options.sizes;
        options.rowCount = options.colors;
        options.MAX_COUNT_ROWS = 1000;
        sizeAutosuggest = function() {
            $(".order-grid-size-text").autocomplete(base_url + options.sizeAutosuggestUrl, {
                width: 120,
                dataType: "json",
                parse: function(handle) {
                    if(handle.status == 'success') {
                        return $.map(handle.sizes, function(size) {
                            return {
                                data: size,
                                value: size.id,
                                result: size.name
                            }
                        });
                    }
                    else {
                        return false;
                    }
                },
                formatItem: function(size) {
                    return size.name;
                },
                multiple: false,
                matchContains: false,
                highlight: false,
                formatted: false
            });
        }
        bindNumericQuantity = function() {
            //$(".ordergrid-qty").autoNumeric('init', {mDec: 0});
        };
        colorAutosuggest = function() {
               	
            $(".order-grid-color-text").blur(function() {
                data = $(this).val();
                curr_item = $(this);
                data_array = data.split(' - ');
                $(this).val(data_array[0]);
                $.post(base_url + 'color/get_by_name', {
                    color_name:data_array[0]
                    }, function(data){
                    if(data.status == ic.SUCCESS) {
                        //$(curr_item).next().miniColors('value',data.color.color_code);
                        //$(curr_item).next().miniColors('disabled', true);
                        // if($("#association-table").length) {
                        //     var colors = getColors();
                        //     ic.quicklay.addOrderColor(colors);
                        // }
                    }
                    else {
                        $(curr_item).next().miniColors('disabled', false);
                    }
                    if($("#association-table").length) {
                        var colors = getColors();
                        ic.quicklay.addOrderColor(colors);
                    }
                }, 'json');
            });
            
            $(".ordergrid-qty").blur(function() {
                calculateTotals();
            });
                        
        }

        var getColors = function() {
            var colorsArray = new Array();
            $(".order-grid-color-text").each(function() {
                console.log($(this).val());
                if($(this).val() != "")
                    colorsArray.push($(this).val());
            });
            return colorsArray;
        };
        
        var calculateTotals = function(){
            var grandTotal = 0;
            $("div[id^='total-col-quantity-']").each(function(){
                var indexArray = $(this).attr('id').split('-'),
                    colNumber = indexArray[indexArray.length - 1],
                    colSum = 0;
                    $("input[id^='ordergrid-qty-'][id$='-" + colNumber + "']").each(function(){
                        var val = parseInt($(this).val())
                        if(isNaN(val))val = 0;
                        colSum += val;
                    });
                    if(colSum > 0)$(this).html(colSum);
                    else $(this).html('');
                    grandTotal += colSum;
            });
            $("div[id^='total-row-quantity-']").each(function(){
                var indexArray = $(this).attr('id').split('-'),
                    rowNumber = indexArray[indexArray.length - 1],
                    rowSum = 0;
                    $("input[id^='ordergrid-qty-" + rowNumber + "-']").each(function(){
                        var val = parseInt($(this).val())
                        if(isNaN(val))val = 0;
                        rowSum += val;
                    });
                    if(rowSum > 0)$(this).html(rowSum);
                    else $(this).html('');
            });
            if(grandTotal > 0)$("#grand-total-quantity").html(grandTotal);
            else $("#grand-total-quantity").html('');
        };
        
        renderFullGrid = function(elem) {
            obj = $(elem);
            obj.html('');
            var hide_add_link = obj.hasClass('hide_add_link');
            obj.html('<table class="ordergrid"><thead></thead><tbody></tbody></table>');
            //$('thead', obj).html('<tr><td colspan="2"></td><td colspan="100"><div id="ordergrid-new-start-col"><a class="new-column-start" href="#Add new column"">'+ ic.lang.ordergrid.add_new_column +'</a></div><div id="ordergrid-new-end-col"><a href="#Add new column" class="new-column-end">'+ ic.lang.ordergrid.add_new_column +'</a></div></td></tr>');
            if(hide_add_link == true){
                    $('thead', obj).html('<tr><td colspan="2"></td><td colspan="100"></td></tr>'); 
                }else{
                    $('thead', obj).html('<tr><td colspan="2"></td><td colspan="100"><div id="ordergrid-new-start-col"><a class="new-column-start" href="#Add new column"">add_new_column </a></div><div id="ordergrid-new-end-col"><a href="#Add new column" class="new-column-end">add_new_column</a></div></td></tr>');
            }
            $('thead', obj).append('<tr><td colspan="2"></tr>');
            for(j=0;j<options.sizes;j++) {
                $('thead tr:eq(1)', obj).append('<td style="padding:1px;text-align:center;" nowrap>size'+ (j+1) + '</td>');
            }
            $('thead tr:eq(1)', obj).append('<td style="padding:1px;" nowrap>total</td>');
            
            $('thead', obj).append('<tr><td colspan="2"></tr>');
            for(j=0;j<options.sizes;j++) {
                $('thead tr:eq(2)', obj).append('<td style="padding:1px;" nowrap><input type="text" name="order-grid-size-text" style="width:95%" class="order-grid-size-text" autocomplete="off"></td></td>');
            }

            for(i=0;i<options.colors;i++) {
                $('tbody', obj).append('<tr></tr>');
                $('tbody tr:eq(' + i + ')', obj).append('<td style="padding:1px;" nowrap class="bold">colors' + (i+1) + '</td>');
                $('tbody tr:eq(' + i + ')', obj).append('<td style="padding:1px;min-width:155px;" class="" nowrap><input type="text" class="order-grid-color-text" style="width:90px;padding-right:5px;" /><input type="hidden" class="order-grid-colorpicker-text colors" size="7" /></td>');
                for(j=0;j<options.sizes;j++) {
                    $('tbody tr:eq(' + i + ')', obj).append('<td style="padding:1px;"><input type="text" id="ordergrid-qty-' + (i+1) + '-' + j + '" old_value="0" class="ordergrid-qty ordergrid-qty-' + (i+1) + '" style="width:95%" alt="p8x3p0U" autocomplete="off" /></td>');
                }
                $('tbody tr:eq(' + i + ')', obj).append('<td style="padding:1px;" nowrap><div style="font-weight: bold;border: 1px solid #7F9DB9;height: 28px; min-width: 52px;text-align: right;" class="total-quantity total-row-quantity" id="total-row-quantity-' + (i+1) + '"></div></td>');
            }
            //$('tbody', obj).append('<tr><td style="padding:1px;" class="bold" colspan="100"><a href="##" class="new-row">Add new color</a></td></tr>');            
            
            $('tbody', obj).append('<tr></tr>');
            if(options.newColorLink) {
                $('tbody tr:eq(' + options.colors + ')', obj).append('<td style="padding:1px;" colspan="2" class="bold"><a href="##" class="new-row">add_new_color</a><span style="float:right;">total</span></td>');            
            }
            else {
                $('tbody tr:eq(' + options.colors + ')', obj).append('<td style="padding:1px;" colspan="2" class="bold"><span style="float:right;">total</span></td>');            
            }
            
            for(j=0;j<options.sizes;j++) {
               $('tbody tr:eq(' + options.colors + ')', obj).append('<td style="padding:1px;"><div style="font-weight: bold;border: 1px solid #7F9DB9;height: 28px; min-width: 52px;text-align: right;" class="total-quantity total-col-quantity" id="total-col-quantity-' + j + '"></div></td>');
            }
            $('tbody tr:eq(' + options.colors + ')', obj).append('<td style="padding:1px;" nowrap><div style="font-weight: bold;border: 1px solid #7F9DB9;height: 28px; min-width: 52px;text-align: right;" class="total-quantity" id="grand-total-quantity"></div></td>');
            
//            $(".colors").miniColors({
//                    change: function(hex, rgb) {
//                            //$("#console").prepend('HEX: ' + hex + ' (RGB: ' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ')<br />');
//                    }
//            });
//            sizeAutosuggest();
            colorAutosuggest();
            bindNumericQuantity();

            var newColumnStartLink = $('.new-column-start', obj);
            var newColumnEndLink = $('.new-column-end', obj);
            var newRowLink = $('.new-row', obj);

            // populate data
            if(options.dataUrl != '') {
                $.getJSON(options.dataUrl, options.dataParams, function(data){
                    if(data.sizes.length * data.colors.length != data.quantity.length) {
                        alert('Invalid json data');
                        return;
                    }
                    if(data.sizes.length > 10) {
                        for(i=10; i<data.sizes.length;i++) {
                            $('.new-column-start', obj).trigger('click');
                        }
                    }
                    if(data.colors.length > 5) {
                        for(i=5; i<data.colors.length;i++) {
                            $('.new-row', obj).trigger('click');
                        }
                    }
                    var i =0;
                    $('.order-grid-size-text', obj).each(function(){
                        if(i < data.sizes.length)
                            $(this).val(data.sizes[i].name);
                        i++;
                    });
                    i =0;
                    $('.order-grid-color-text', obj).each(function(){
                        if(i < data.colors.length) {
                            $(this).val(data.colors[i].name);
                            $(this).next().miniColors('value',data.colors[i].color_code);
                            $(this).next().miniColors({
                                disabled:true,
                                value:data.colors[i].color_code
                            });
                        }
                        else {
                            $(this).next().miniColors({disabled:false});
                        }
                        i++;
                    });

                    for (i=0; i < data.colors.length; i++) {
                        var j = 0;
                        console.log(data.quantity);
                        $('.ordergrid-qty-' + (i+1), obj).each(function(){
                            if(j < data.sizes.length){
                                $(this).val(data.quantity[i*data.sizes.length+j]);
                                $(this).attr('old_value',data.quantity[i*data.sizes.length+j]);
                            }
                            j++;
                        });
                    }
                    //disable the size text box if enter actual is done
                    for (i=0; i < data.colors.length; i++) {
                        var j = 0;
                        $('.order-grid-size-text', obj).each(function(){
                            if(j < data.sizes.length){
                                cut_value = data.actual_cut_quantity[i*data.sizes.length+j];
                                if(cut_value > 0){
                                    $(this).attr("disabled", true);
                                }
                                j++;
                            }
                        });
                    }
                    //disable the color text box if enter actual is done
                    i =0;
                    $('.order-grid-color-text', obj).each(function(){
                        for (var j=0; j < data.sizes.length; j++) {
                            cut_value = data.actual_cut_quantity[i*data.sizes.length+j];
                            if(cut_value > 0){
                                $(this).attr("disabled", true);
                            }
                        }
                        i++;
                    });
                    //disable the color text box if fabric association is done
                    $('.order-grid-color-text', obj).each(function(){
                        if(data.is_fabric_associated == true){
                            $(this).attr("disabled", true);
                        }       
                    });

                    //disable color text box for other parts except main part
                    $('.order-grid-color-text', obj).each(function(){
                        if(data.is_main_part == false){
                            // $(this).attr("disabled", true);
                            $('.order-grid-color-text').attr("disabled", true);
                        }       
                    });

                    //disable size text box for other parts except main part
                    for (i=0; i < data.colors.length; i++) {
                        var j = 0;
                        $('.order-grid-size-text', obj).each(function(){
                            if(j < data.sizes.length){
                                if(data.is_main_part == false){
                                    // $(this).attr("disabled", true);
                                    $('.order-grid-size-text').attr("disabled", true);
                                }
                                j++;
                            }
                        });
                    }
                    //disable color text box for target_po_quantity
                    if(data.order_type == 'target' || data.order_type == 'original'){
                        $('.order-grid-color-text').attr("disabled", true);
                    }       

                    //disable size text box for for target_po_quantity
                    if(data.order_type == 'original'){
                        if(data.is_cutplan_and_marker_exists == false)
                            $('.order-grid-size-text').attr("disabled", true);
                    }
                    if(data.order_type == 'target'){
                            $('.order-grid-size-text').attr("disabled", true);
                    }
                        
                });

            }
            else {
                // $(".colors").miniColors({
                //     change: function(hex, rgb) {
                //             //$("#console").prepend('HEX: ' + hex + ' (RGB: ' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ')<br />');
                //     }
                // });
            }
            newColumnStartLink.click(function() {
                var disable_size_and_color = obj.hasClass('disable_size_and_color');
                $("tr:eq(1) td:nth-child(1)", obj).after('<td style="padding:1px;" nowrap>' + ic.lang.ordergrid.size + 1 +'</td>');
                if(disable_size_and_color == true){
                    $("tr:eq(2) td:nth-child(1)", obj).after('<td style="padding:1px;"><input type="text" style="width:95%" class="order-grid-size-text" disabled autocomplete="off"></td>');
                }else{
                    $("tr:eq(2) td:nth-child(1)", obj).after('<td style="padding:1px;"><input type="text" style="width:95%" class="order-grid-size-text" autocomplete="off"></td>');
                }
                var row_count = $("input[id^='ordergrid-qty-'][id$='-0']").length,
                    inputRowArray = $("input[id^='ordergrid-qty-1-']").map(function(){
                        return parseInt($(this).attr('id').split("-").pop());
                    }).sort(function(a,b){return b-a}),
                    changeHeader = true;                
                
                for(var i=1; i <= row_count; i++){
                    inputRowArray.each(function(index, e){
                        $("#ordergrid-qty-" + i + "-" + e).attr('id', "ordergrid-qty-" + i + "-" + (e + 1));
                        if(changeHeader){
                            $("tr:eq(1) td:nth-child(" + (3 + e) + ")", obj).html( ic.lang.ordergrid.size + (2 + e));
                            $("#total-col-quantity-" + e).attr('id', "total-col-quantity-" + (e + 1));
                        }
                    });
                    changeHeader = false;
                    $("tr:eq(" + (2+i) + ") td:nth-child(2)", obj).after('<td style="padding:1px;"><input type="text" id="ordergrid-qty-' + i + '-0" class="ordergrid-qty ordergrid-qty-' + i + '" style="width:95%" alt="p8x3p0U" autocomplete="off"></td>');                    
                }
                $('tbody tr:last td:nth-child(1)', obj).after('<td style="padding:1px;"><div style="font-weight: bold;border: 1px solid #7F9DB9;height: 13px; min-width: 52px;text-align: right;" class="total-quantity total-col-quantity" id="total-col-quantity-0"></div></td>');
                options.startIndex--;
                // sizeAutosuggest();
                bindNumericQuantity();
                attachSizeQtyPasteEvent();
                $(".ordergrid-qty").unbind('blur').blur(function() {
                    calculateTotals();
                });
            });
            newColumnEndLink.click(function() {
                var disable_size_and_color = obj.hasClass('disable_size_and_color');
                var row_count = $("input[id^='ordergrid-qty-'][id$='-0']").length,
                    inputRowArray = $("input[id^='ordergrid-qty-1-']").map(function(){
                        return parseInt($(this).attr('id').split("-").pop());
                    }).sort(function(a,b){return b-a});                
                $("tr:eq(1) td:nth-child(" + (1 + inputRowArray.length) + ")", obj).after('<td style="padding:1px;" nowrap>'+ ic.lang.ordergrid.size + (1 + inputRowArray.length) + '</td>');
                if(disable_size_and_color == true){
                    $("tr:eq(2) td:nth-child(" + (1 + inputRowArray.length) + ")", obj).after('<td style="padding:1px;"><input type="text" style="width:95%" class="order-grid-size-text" disabled autocomplete="off"></td>');
                }else{
                    $("tr:eq(2) td:nth-child(" + (1 + inputRowArray.length) + ")", obj).after('<td style="padding:1px;"><input type="text" style="width:95%" class="order-grid-size-text" autocomplete="off"></td>');
                }
                for(var i=1; i <= row_count; i++){
                    $("tr:eq(" + (2+i) + ") td:nth-child(" + (inputRowArray.length + 2) + ")", obj).after('<td style="padding:1px;"><input type="text" id="ordergrid-qty-' + i + '-' + inputRowArray.length + '" class="ordergrid-qty ordergrid-qty-' + i + '" style="width:95%" alt="p8x3p0U" autocomplete="off"></td>');                    
                }
                $('tbody tr:last td:nth-child(' + (inputRowArray.length + 1) + ')', obj).after('<td style="padding:1px;"><div style="font-weight: bold;border: 1px solid #7F9DB9;height: 13px; min-width: 52px;text-align: right;" class="total-quantity total-col-quantity" id="total-col-quantity-' + (inputRowArray.length) + '"></div></td>');
                options.startIndex--;
                // sizeAutosuggest();
                bindNumericQuantity();                
                attachSizeQtyPasteEvent();
                $(".ordergrid-qty").unbind('blur').blur(function() {
                    calculateTotals();
                });
            });
            newRowLink.click(function() {
                var disable_size_and_color = obj.hasClass('disable_size_and_color');
                var row_count = $("input[id^='ordergrid-qty-'][id$='-0']").length,
                    inputRowArray = $("input[id^='ordergrid-qty-1-']").map(function(){
                        return parseInt($(this).attr('id').split("-").pop());
                    }).sort(function(a,b){return b-a}),
                    col_count = inputRowArray.length;
                
                $("tr:last", obj).before("<tr></tr>");
                $("tr:eq(" + (row_count + 3) + ")", obj).html('<td style="padding:1px;" class="bold">'+ ic.lang.ordergrid.colors + (row_count + 1) + '</td>');
                if(disable_size_and_color == true){
                    $("tr:eq(" + (row_count + 3) + ") td:nth-child(1)", obj).after('<td class="" style="padding:1px;"><input type="text" name="order-grid-color-text" disabled class="order-grid-color-text" style="width:90px;padding-right:5px;" id="order-grid-color-text_' + (row_count + 1) + '"><input type="hidden" class="order-grid-colorpicker-text colors" size="7" /></td>');
                }else{
                    $("tr:eq(" + (row_count + 3) + ") td:nth-child(1)", obj).after('<td class="" style="padding:1px;"><input type="text" name="order-grid-color-text" class="order-grid-color-text" style="width:90px;padding-right:5px;" id="order-grid-color-text_' + (row_count + 1) + '"><input type="hidden" class="order-grid-colorpicker-text colors" size="7" /></td>');
                }
                $(".colors").miniColors({
                    change: function(hex, rgb) {
                    //$("#console").prepend('HEX: ' + hex + ' (RGB: ' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ')<br />');
                    }
                });
                for(var i = 0; i < col_count; i++) {
                    $("tr:eq(" + (row_count + 3) + ") td:nth-child(" + (i + 2) +")", obj).after('<td style="padding:1px;"><input type="text" id="ordergrid-qty-' + (row_count + 1) + '-' + i + '" class="ordergrid-qty ordergrid-qty-' + (row_count + 1) + '" style="width:95%" alt="p8x3p0U" autocomplete="off"></td>');
                }
                $('tr:eq(' + (row_count + 3) + ') td:last', obj).after('<td style="padding:1px;" nowrap><div style="font-weight: bold;border: 1px solid #7F9DB9;height: 28px; min-width: 52px;text-align: right;" class="total-quantity total-row-quantity" id="total-row-quantity-' + (row_count + 1) + '"></div></td>');
                options.rowCount++;
                colorAutosuggest();
                // sizeAutosuggest();
                bindNumericQuantity();                
                attachSizeQtyPasteEvent();
                $(".ordergrid-qty").unbind('blur').blur(function() {
                    calculateTotals();
                });
            });

            attachOrderSizePasteEvent = function() {
                $('.order-grid-size-text').on('paste', function () {
                  var element = this;
                  setTimeout(function () {
                    var text = $(element).val();
                    $(element).val('');
                    //$('.order-grid-size-text').index(this);
                    var txtArray = text.split(/\s+/);
                    //var tmpArray = $(element).attr('id').split('B');
                    //var elemId = parseInt(tmpArray[1]);
                    // var maxLimit = parseInt(elemId) + txtArray.length - 1;
                    // if(maxLimit > $('.splice-length').length) {
                    //     var requiredRows = maxLimit -  $('.splice-length').length;
                    //     for(var i = 0; i < requiredRows; i++) {
                    //         addRow();
                    //     }    
                    //     attachPasteEvents();
                    // }
                    if($('.order-grid-size-text').size() < txtArray.length) {
                        requiredBoxes = txtArray.length - $('.order-grid-size-text').size();
                        for(var i = 0; i < requiredBoxes; i++) {
                            newColumnEndLink.click();
                            attachOrderSizePasteEvent();
                            attachSizeQtyPasteEvent();
                        }
                    }
                    for(var counter = 0; counter < txtArray.length; counter++) {
                            var pattern = /^\d*[a-zA-Z,-_\.\'\"\*\/\\\s\“]*?$/,
                             //var pattern = /^\d*(\.\d{1,5}[a-zA-Z]+)?(-\d+(\.\d{1,5})?)?$/,
                            isValid = txtArray[counter].match(pattern);
                        if(!isValid){
                          $(this).val("");
                          alert(ic.lang.style.only_numbers_alphabet_comma_hyphon_dot_with_proper_formatting_please)
                          return false;
                        }else {
                              var value= txtArray[counter];
                              console.log(value);
                              $('.order-grid-size-text:eq(' + (counter) + ')').val(value);                   
                          }
                    }
                    // do something with text
                  }, 100);
                });
            }
            attachOrderSizePasteEvent();
            attachSizeQtyPasteEvent = function() {
                $('.ordergrid-qty').on('paste', function () {
                  var element = this;
                  setTimeout(function () {
                    var text = $(element).val();
                    tmpArray = $(element).attr('id').split('-');
                    var colorIndex = tmpArray[tmpArray.length-2];
                    $(element).val('');
                    //$('.order-grid-size-text').index(this);
                    var txtArray = text.split(/\s+/);
                    //var tmpArray = $(element).attr('id').split('B');
                    //var elemId = parseInt(tmpArray[1]);
                    // var maxLimit = parseInt(elemId) + txtArray.length - 1;
                    // if(maxLimit > $('.splice-length').length) {
                    //     var requiredRows = maxLimit -  $('.splice-length').length;
                    //     for(var i = 0; i < requiredRows; i++) {
                    //         addRow();
                    //     }    
                    //     attachPasteEvents();
                    // }
                    if($('.ordergrid-qty-' + colorIndex).size() < txtArray.length) {
                        requiredBoxes = txtArray.length - $('.ordergrid-qty-' + colorIndex).size();
                        for(var i = 0; i < requiredBoxes; i++) {
                            newColumnEndLink.click();
                            attachOrderSizePasteEvent();
                            attachSizeQtyPasteEvent();
                        }
                    }
                    for(var counter = 0; counter < txtArray.length; counter++) {
                            var pattern = /^\d*(\d{1,5})?(-\d+(\d{1,5})?)?$/,
                             //var pattern = /^\d*(\.\d{1,5}[a-zA-Z]+)?(-\d+(\.\d{1,5})?)?$/,
                            isValid = txtArray[counter].match(pattern);
                        if(!isValid){
                          $(this).val("");
                          alert(ic.lang.style.only_numbers_with_proper_formatting_please)
                          return false;
                        }else {
                              var value= txtArray[counter];
                              console.log(value);
                              $('.ordergrid-qty-' + colorIndex + ':eq(' + (counter) + ')').val(value);                   
                          }
                    }
                    // do something with text
                  }, 100);
                });
            }
            attachSizeQtyPasteEvent();
        }

        renderReadonlyGrid = function(elem) {
            obj = $(elem);
            obj.html('<table class="ordergrid"><thead></thead><tbody></tbody></table>');                        
            $.getJSON(options.dataUrl, options.dataParams, function(data){
                $('thead', obj).append('<tr><td colspan="3"></td></tr>');
                for (i=0; i < data.sizes.length; i++) {
                    $('thead tr:eq(0)', obj).append('<td style="padding:1px;" nowrap>' + ic.lang.ordergrid.size  + (i+1) + '</td>');
                }
                $('thead tr:eq(0)', obj).append('<td style="padding:1px;" nowrap></td>');
                $('thead', obj).append('<tr><td colspan="3"></tr>');
                for (i=0; i < data.sizes.length; i++) {
                    $('thead tr:eq(1)', obj).append('<td style="padding:1px;" nowrap>' + data.sizes[i].name + '</td>');
                }
                $('thead tr:eq(1)', obj).append('<td style="padding:1px;" nowrap>'+ic.lang.ordergrid.total+'</td>');

                var size_total = new Array();
                for (i=0; i <= data.sizes.length; i++) {
                    size_total[i] = 0;
                }
                for (i=0; i < data.colors.length; i++) {
                    $('tbody', obj).append('<tr></tr>');
                    $('tbody tr:eq(' + (i) + ')', obj).append('<td style="padding:1px;width:40px;" nowrap class="bold">'+ ic.lang.ordergrid.colors + (i+1) + ' :</td>');
                    $('tbody tr:eq(' + (i) + ')', obj).append('<td style="padding:1px;width:40px;" class="" nowrap>' + data.colors[i].name +'</td>');
                    $('tbody tr:eq(' + (i) + ')', obj).append('<td style="padding:1px;width:40px;"><input type="hidden" class="order-grid-colorpicker-text colors" size="7" value="' + data.colors[i].color_code + '" /></td>');
                    $('.colors', obj).miniColors({
                        disabled:true
                    });
                    var total = 0;
                    for(j=0;j<data.sizes.length;j++) {
                        $('tbody tr:eq(' + (i) + ')', obj).append('<td style="padding:1px;">' + data.quantity[j] + '</td>');
                        total = total + parseInt(data.quantity[j]);
                        size_total[j] = size_total[j] + parseInt(data.quantity[j]);
                    }
                    size_total[j] = size_total[j] + total;
                    $('tbody tr:eq(' + (i) + ')', obj).append('<td style="padding:1px;">' + total + '</td>');
                }
                $('tbody', obj).append('<tr></tr>');
                $('tbody tr:eq(' + (i) + ')', obj).append('<td style="padding:1px;width:40px;" nowrap class="bold">'+ic.lang.ordergrid.total+'</td>');
                $('tbody tr:eq(' + (i) + ')', obj).append('<td style="padding:1px;width:40px;" class="bold" nowrap></td>');
                $('tbody tr:eq(' + i + ')', obj).append('<td style="padding:1px;width:40px;"></td>');
                for(j=0;j<size_total.length;j++) {
                    $('tbody tr:eq(' + i + ')', obj).append('<td style="padding:1px;">' + size_total[j] + '</td>');
                }
            });
        }

        renderContentOnlyGrid = function(elem) {
            obj = $(elem);
            obj.html('<table class="ordergrid"><thead></thead><tbody></tbody></table>');
            $.getJSON(options.dataUrl, options.dataParams, function(data){
                if(data.sizes.length == 0 || data.colors.length == 0) {
                    jError(ic.lang.order.order_grid_does_not_exist);
                    ic.order.editOrderQuantityGrid(options.dataParams.order_id);
                    return false;
                }
                $('thead', obj).append('<tr><td colspan="3"></tr>');
                for (i=0; i < data.sizes.length; i++) {
                    $('thead tr:eq(0)', obj).append('<td style="padding:1px;" nowrap>'+ ic.lang.ordergrid.size + (i+1) + '</td>');
                }
                $('thead', obj).append('<tr><td colspan="3"></tr>');
                for (i=0; i < data.sizes.length; i++) {
                    $('thead tr:eq(1)', obj).append('<td style="padding:1px;" nowrap>' + data.sizes[i].name + '<input type="hidden" value="' + data.sizes[i].name + '" name="order-grid-size-text" style="width:95%" class="order-grid-size-text" autocomplete="off"></td>');
                }
                for (i=0; i < data.colors.length; i++) {
                    $('tbody', obj).append('<tr></tr>');
                    $('tbody tr:eq(' + i + ')', obj).append('<td style="padding:1px;width:40px;" nowrap class="bold">'+ ic.lang.ordergrid.colors + (i+1) + ' :</td>');
                    $('tbody tr:eq(' + i + ')', obj).append('<td style="padding:1px;width:40px;" class="" nowrap>' + data.colors[i].name +'<input value="' + data.colors[i].name + '" type="hidden" class="order-grid-color-text"/></td>');
                    $('tbody tr:eq(' + i + ')', obj).append('<td style="padding:1px;width:40px;"><input type="hidden" class="order-grid-colorpicker-text colors" size="7" value="' + data.colors[i].color_code + '" /></td>');
                    $('.colors', obj).miniColors({
                        disabled: true
                    });
                    for(j=0;j<data.sizes.length;j++) {
                        if(undefined != data.quantity[(i*data.sizes.length)+j])
                            $('tbody tr:eq(' + i + ')', obj).append('<td style="padding:1px;"><input value="' + data.quantity[(i*data.sizes.length)+j] + '" type="text" class="ordergrid-qty ordergrid-qty-' + (i+1) + '" style="width:95%" alt="p8x3p0U" autocomplete="off"></td>');
                        else
                            $('tbody tr:eq(' + i + ')', obj).append('<td style="padding:1px;"><input value="0" type="text" class="ordergrid-qty ordergrid-qty-' + (i+1) + '" style="width:95%; border:1px solid #000;" alt="p8x3p0U" autocomplete="off"></td>');
                    }
                }
            });
            bindNumericQuantity();
        }
        return this.each(function() {
            if(options.type == 'full') {
                renderFullGrid(this);
            }
            else if(options.type == 'readonly'){
                if(options.dataUrl == '') {
                    alert('Please provide data url for ordergrid.');
                }
                renderReadonlyGrid(this);
            }
            else if(options.type == 'content') {
                if(options.dataUrl == '') {
                    alert('Please provide data url for ordergrid.');
                }
                renderContentOnlyGrid(this);
            }
            else {
                renderFullGrid(this);
            }
        });
    };
})(jQuery);